import { SchemaField } from './fields'

export class Schema {
    protected fields: SchemaField<unknown>[]

    constructor() {
        this.fields = []
    }

    addField(field: SchemaField<unknown>): this {
        this.fields.push(field)
        return this
    }

    getField(field: string): SchemaField<unknown> {
        const index = this.fields.findIndex((field1) => {
            return field === field1.getName()
        })
        return this.fields[index]
    }

    forEach(func: (field: SchemaField<unknown>) => void): void {
        for (const field of this.fields) {
            func(field)
        }
    }
}
