import { Schema } from './schema'
import { SchemaFieldNumber, SchemaFieldString } from './fields'

/**
 * Entity is an interface for pre-defined models that have a pre-set schema, but are very expandable and dynamic
 */
export class Entity {
    protected _schema: Schema

    constructor(data?: object) {
        this._schema = this.getSchema(new Schema())
        if (data) {
            this.import(data)
        } else {
            this.import({})
        }
    }

    /**
     * Retrieve the schema for the object
     * @param schema
     */
    getSchema(schema: Schema): Schema {
        schema.addField(new SchemaFieldString('_schemaType', this.getSchemaName()))
        schema.addField(new SchemaFieldNumber('_schemaVersion', this.getSchemaVersion()))
        return schema
    }

    /**
     * Import a data object into the structured object
     * @param data
     */
    import(data: object): void {
        this._schema.forEach(field => {
            field.import(data, this)
        })
    }

    /**
     * Export the structured data
     * @param data
     */
    export(data?: object): unknown {
        const dst = (data) ? data : {}
        this._schema.forEach(field => {
            field.export(this, dst)
        })
        return dst
    }

    getSchemaName(): string {
        return 'object'
    }
    getSchemaVersion(): number {
        return 1
    }
}
