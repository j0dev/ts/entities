import { Entity } from '../entity'

/**
 * Entity field definition object
 */
export abstract class SchemaField<T> {
    /**
     * key of field inside the entity
     */
    protected fieldName: string

    /**
     * Default value of the field
     */
    protected defaultValue: T | null

    constructor(fieldName: string, defaultValue?: T) {
        this.fieldName = fieldName
        this.defaultValue = (defaultValue)? defaultValue : null
    }

    /**
     * Get the name / key of the field
     */
    getName(): string {
        return this.fieldName
    }

    /**
     * Get the default value for this field
     */
    getDefault(): T | null {
        return this.defaultValue
    }

    /**
     * Check if a value is equal to the default value for this field
     * @param value
     */
    isDefault(value: T | null): boolean {
        return value === this.defaultValue
    }

    /**
     * Get the type name for this field
     */
    abstract getType(): string

    /**
     * Logic for importing this field from a source object to a destination Entity
     */
    import(src: object, dst: Entity): void {
        // check if the source contains a value for us
        if (Object.keys(src).indexOf(this.fieldName) !== -1) {
            Reflect.set(dst, this.fieldName, Reflect.get(src, this.fieldName))
        } else {
            Reflect.set(dst, this.fieldName, this.defaultValue)
        }
    }

    /**
     * Logic for exporting this field from an Entity to a regular object
     */
    export(src: Entity, dst: object): void {
        Reflect.set(dst, this.fieldName, Reflect.get(src, this.fieldName))
    }
}
