import { SchemaField } from './schema-field'
import { Entity } from '../entity'
import { Dayjs } from 'dayjs'
import { Duration } from 'dayjs/plugin/duration'
import * as dayjs from 'dayjs'

/**
 * DateTime schema field
 */
export class SchemaFieldDateTime extends SchemaField<Dayjs> {
    constructor(fieldName: string, defaultValue?: Dayjs | string) {
        if (typeof(defaultValue) === 'string') {
            defaultValue = new Dayjs(defaultValue)
        }
        super(fieldName, defaultValue)
    }

    override getType(): string {
        return 'datetime'
    }

    override isDefault(value: Dayjs | null): boolean {
        if (this.defaultValue && value) {
            return this.defaultValue.isSame(value)
        } else {
            return this.defaultValue == value
        }
    }

    override import(src: object, dst: Entity) {
        // TODO: check if already a dayjs object, otherwise transform

        if (Object.keys(src).indexOf(this.fieldName) !== -1) {
            Reflect.set(dst, this.fieldName, dayjs(Reflect.get(src, this.fieldName)))
        } else {
            Reflect.set(dst, this.fieldName, this.defaultValue)
        }
    }

    override export(src: Entity, dst: object) {
        let value = Reflect.get(src, this.fieldName)
        value = (value) ? value.toISOString() : null
        Reflect.set(dst, this.fieldName, value)
    }
}

// TODO: (just) Date
// TODO: (just) Time

/**
 * Duration schema field
 */
export class SchemaFieldDuration extends SchemaField<Duration> {

    constructor(fieldName: string, defaultValue?: Duration | string) {
        if (typeof(defaultValue) === 'string') {
            defaultValue = dayjs.duration(defaultValue)
        }
        super(fieldName, defaultValue)
    }

    override getType(): string {
        return 'duration'
    }

    override isDefault(value: Duration | null): boolean {
        if (this.defaultValue && value) {
            return this.defaultValue.toISOString() == value.toISOString()
        } else {
            return this.defaultValue == value
        }
    }

    override import(src: object, dst: Entity) {
        if (Object.keys(src).indexOf(this.fieldName) !== -1) {
            Reflect.set(dst, this.fieldName, dayjs.duration(Reflect.get(src, this.fieldName)))
        } else {
            Reflect.set(dst, this.fieldName, this.defaultValue)
        }
    }

    override export(src: Entity, dst: object) {
        let value = Reflect.get(src, this.fieldName)
        value = (value) ? value.toISOString() : null
        Reflect.set(dst, this.fieldName, value)
    }
}
