import { Entity } from '../entity'
import { SchemaField } from './schema-field'
import { Schema } from '../schema'

/**
 * Entity schema field
 * Used for nesting Entities
 */
export class SchemaFieldEntity extends SchemaField<Entity> {
    /**
     * Type of the entity
     * Used to instantiate new entities
     */
    entityType: typeof Entity

    constructor(fieldName: string, entityType: typeof Entity, defaultValue?: Entity) {
        super(fieldName, defaultValue)
        this.entityType = entityType
    }

    override getType(): string {
        return new this.entityType().getSchemaName()
    }

    override isDefault(value: Entity): boolean {
        let answer = true
        value.getSchema(new Schema()).forEach((field) => {
            if (!field.isDefault(Reflect.get(value, field.getName()))) {
                answer = false
            }
        })
        return answer
    }

    override import(src: object, dst: Entity) {
        // check if the source contains our key
        if (Object.keys(src).indexOf(this.fieldName) !== -1) {
            // check for null values, otherwise import into entity
            if (Reflect.get(src, this.fieldName)) {
                const srcValue = Reflect.get(src, this.fieldName)
                // check if there is already an Entity in the destination, if so, import otherwise create
                if (Reflect.get(dst, this.fieldName)) {
                    Reflect.get(dst, this.fieldName).import(srcValue)
                } else {
                    Reflect.set(dst, this.fieldName, new this.entityType(srcValue))
                }
            } else {
                // got null
                Reflect.set(dst, this.fieldName, null)
            }
        } else {
            // got undefined, setting default
            Reflect.set(dst, this.fieldName, this.defaultValue)
        }
    }

    override export(src: Entity, dst: object) {
        // check for null values
        if (Reflect.get(src, this.fieldName)) {
            Reflect.set(dst, this.fieldName, Reflect.get(src, this.fieldName).export())
        } else {
            Reflect.set(dst, this.fieldName, null)
        }
    }
}

/**
 * Entity array schema field
 * Used for nesting Entities
 */
export class SchemaFieldEntityArray extends SchemaField<Array<Entity>> {
    /**
     * Type of the entity
     * Used to instantiate new entities
     */
    entityType: typeof Entity

    constructor(fieldName: string, entityType: typeof Entity, defaultValue?: Entity[]) {
        const dValue = (defaultValue) ? defaultValue : []
        super(fieldName, dValue)
        this.entityType = entityType
    }

    override getType(): string {
        return 'array.' + new this.entityType().getSchemaName()
    }

    override isDefault(value: Array<Entity>): boolean {
        return value.length == 0
    }

    override import(src: object, dst: Entity) {
        const arr: Entity[] = []

        if (Object.keys(src).indexOf(this.fieldName) !== -1) {
            for (const item of Reflect.get(src, this.fieldName)) {
                arr.push(new this.entityType(item))
            }
        }
        Reflect.set(dst, this.fieldName, arr)
    }

    override export(src: Entity, dst: object) {
        const arr: object[] = []
        for (const item of Reflect.get(src, this.fieldName)) {
            arr.push(item.export())
        }
        Reflect.set(dst, this.fieldName, arr)
    }
}

/**
 * Entity map schema field
 * Used for nesting Entities
 */
export class SchemaFieldEntityMap extends SchemaField<Map<string, Entity>> {
    /**
     * Type of the entity
     * Used to instantiate new entities
     */
    entityType: typeof Entity

    constructor(fieldName: string, entityType: typeof Entity, defaultValue?: Map<string, Entity>) {
        const dValue = (defaultValue) ? defaultValue : new Map<string, Entity>()
        super(fieldName, dValue)
        this.entityType = entityType
    }

    override getType(): string {
        return 'map.'+new this.entityType().getSchemaName()
    }

    override isDefault(value: Map<string, Entity>): boolean {
        return value.size == 0
    }

    override import(src: object, dst: Entity) {
        const arr = new Map<string, Entity>()

        if (Object.keys(src).indexOf(this.fieldName) !== -1) {
            for (const [key, item] of Reflect.get(src, this.fieldName)) {
                arr.set(key, new this.entityType(item))
            }
        }

        Reflect.set(dst, this.fieldName, arr)
    }

    override export(src: Entity, dst: object) {
        const arr: object = {}
        for (const [key, value] of Reflect.get(src, this.fieldName)) {
            Reflect.set(arr, key, value.export())
        }
        Reflect.set(dst, this.fieldName, arr)
    }
}
