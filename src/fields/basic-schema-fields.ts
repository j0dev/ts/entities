import { SchemaField } from './schema-field'

/**
 * String schema field
 */
export class SchemaFieldString extends SchemaField<string> {
    constructor(fieldName: string, defaultValue?: string) {
        super(fieldName, defaultValue)
    }

    override getType(): string {
        return 'string'
    }
}

/**
 * Number schema field
 */
export class SchemaFieldNumber extends SchemaField<number> {
    constructor(fieldName: string, defaultValue?: number) {
        super(fieldName, defaultValue)
    }

    override getType(): string {
        return 'number'
    }
}

/**
 * Boolean schema field
 */
export class SchemaFieldBoolean extends SchemaField<boolean> {
    constructor(fieldName: string, defaultValue?: boolean) {
        super(fieldName, defaultValue)
    }

    override getType(): string {
        return 'boolean'
    }
}

/**
 * String array schema field
 */
export class SchemaFieldArrayString extends SchemaField<Array<string>> {
    constructor(fieldName: string, defaultValue?: Array<string>) {
        super(fieldName, defaultValue)
    }

    override getType(): string {
        return 'array.string'
    }
}

/**
 * Number array schema field
 */
export class SchemaFieldArrayNumber extends SchemaField<Array<number>> {
    constructor(fieldName: string, defaultValue?: Array<number>) {
        super(fieldName, defaultValue)
    }

    override getType(): string {
        return 'array.number'
    }
}
