Entities javascript library
======

This library provides a set of functionality to work more dynamically and typed with objects. It also adds a schema to objects and import / export to raw json objects for transfer between systems.


## Features
- At runtime typing information, without needing reflection
- pre-defined default values, including a function to check if a value is the default
- Import / Export functions to translate between plain javascript objects (for exchange with external systems)

## Future idea's and upcoming changes
- reusable and standardised import and export of field types  
    In the current iteration the import and export is tied to the SchemaField,  
    In the future this will be decoupled so that the import and export can be defined elsewhere.  
    This would allow modification and reuse of import and export logic without modification to the SchemaFields types. And in turn make collection objects more generic (eg. maps and arrays)
- Validation system for values
